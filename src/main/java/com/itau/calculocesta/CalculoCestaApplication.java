package com.itau.calculocesta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculoCestaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculoCestaApplication.class, args);
	}

}

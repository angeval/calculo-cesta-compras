package com.itau.calculocesta.services;

import com.itau.calculocesta.models.Basket;
import com.itau.calculocesta.models.Item;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class DiscountApplierTests {
    DiscountApplier discount;
    List<Item> itens = new ArrayList<>();
    Basket basket;

    @BeforeEach
    public void inicializarTestes() {
        basket = new Basket(new ArrayList<>());
    }

    @Test
    public void testarApplyMaceIphone(){
        itens.add(new Item("MACBOOK", 1, 100));
        itens.add(new Item("IPHONE", 1, 100));
        basket = new Basket(itens);
        discount = new DiscountApplier();
        double resultado = basket.getAmount() * 0.85;
        discount.apply(basket);
        Assertions.assertEquals(resultado, basket.getAmount(), 0.0001);
    }

    @Test
    public void testarApplyNoteeIphone(){
        itens.add(new Item("NOTEBOOK", 1, 500));
        itens.add(new Item("WINDOWS PHONE", 1, 100));
        basket = new Basket(itens);
        discount = new DiscountApplier();
        double resultado = basket.getAmount() * 0.88;
        discount.apply(basket);
        Assertions.assertEquals(resultado, basket.getAmount(), 0.0001);
    }

    @Test
    public void testarApplyXBox(){
        itens.add(new Item("XBOX", 1, 50));
        basket = new Basket(itens);
        discount = new DiscountApplier();
        double resultado = basket.getAmount() * 0.30;
        discount.apply(basket);
        Assertions.assertEquals(resultado, basket.getAmount(), 0.0001);
    }

    @Test
    public void testarApplyUntil1000OneItem(){
        itens.add(new Item("FREEZER", 1, 1000));
        basket = new Basket(itens);
        discount = new DiscountApplier();
        double resultado = basket.getAmount() * 0.98;
        discount.apply(basket);
        Assertions.assertEquals(resultado, basket.getAmount(), 0.0001);
    }

    @Test
    public void testarApplyMoreThan1000ThreeItensLessThanFive(){
        itens.add(new Item("FREEZER", 1, 5000));
        itens.add(new Item("VENTILADOR", 2, 500));
        basket = new Basket(itens);
        discount = new DiscountApplier();
        double resultado = basket.getAmount() * 0.95;
        discount.apply(basket);
        Assertions.assertEquals(resultado, basket.getAmount(), 0.0001);
    }

    @Test
    public void testarApplyMoreThan1000MoreThanFive(){
        itens.add(new Item("FREEZER", 1, 5000));
        itens.add(new Item("VENTILADOR", 5, 500));
        basket = new Basket(itens);
        discount = new DiscountApplier();
        double resultado = basket.getAmount() * 0.94;
        discount.apply(basket);
        Assertions.assertEquals(resultado, basket.getAmount(), 0.0001);
    }
}

package com.itau.calculocesta.services;

import com.itau.calculocesta.models.Basket;
import com.itau.calculocesta.models.Clock;
import com.itau.calculocesta.models.Item;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class ProposalGeneratorTests {

    @Test
    public void testarDiscountForChristmas(){
        List<Item> itens = new ArrayList<>();
        itens.add(new Item("MACBOOK", 1, 1000));
        Basket basket = new Basket(itens);

        Calendar christmas = new GregorianCalendar(2020, Calendar.DECEMBER, 25);
        Clock fakeClock = Mockito.mock(Clock.class);
        Mockito.when(fakeClock.now()).thenReturn(christmas);

        ProposalGenerator proposalGenerator = new ProposalGenerator(fakeClock);
        double finalAmount = proposalGenerator.calculateDiscount(basket);
        Assertions.assertEquals(1000*0.15, finalAmount, 0.0001);
    }
}
